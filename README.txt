------------------------
Projet Semmeca
elisabeth.piotelat@limsi.fr
----------------------------

Ce projet utilise GRR pour la gestion de séminaires. 

Il a été développé pour des besoins particuliers de 2 labos (LISN + FAST) et nécessite par conséquent des adaptations importantes pour être utilisé dans un autre contexte. 


---------------------------
Les répertoires
--------------------------

gestion : contient les fichiers php qui seront déplacés sur le serveur web et accessibles uniquement par les organisateurs de séminaires. 
  - mailing.sh envoie à différentes listes créées sur sympa (on fait une annonce large aux chercheurs du domaine une seule fois, mais on envoie des rappels à ceux du campus)
  - verification.sh est envoyé à la personne qui invite pour qu'elle signale d'éventuelles erreurs

scripts : contient les scripts bash permettant l'envoi de mails


