<?php

// Création des annonces des prochains séminaires

function debut_limsi() {
    global $dossier, $entete, $index, $sortie, $out;
    $annonceweb = "$dossier/$index";
    copy($entete, $annonceweb);
    $f_dest = fopen($out, "w");
    fwrite($f_dest, "<h2>Prochains s&eacute;minaires</h2><ul>\n");
    return $f_dest;
}

function debut_mail() {
    global $entetem, $dossier, $message;
    $annoncemail = "$dossier/$message";
    copy($entetem, $annoncemail);
    $f_dest = fopen($annoncemail, "a+");
    $sep = "*****************************************************************\n";
    fwrite($f_dest, $sep);
    return $f_dest;
}

function signature_mail() {

    global $dossier, $message, $actulimsi, $urlout, $f_mail, $entetem;
    $annoncemail = "$dossier/$message";
    $semdelannee = sempasse(time());
    $sep = "*****************************************************************";

    fwrite($f_mail, "\n\n$sep\n\n");
    cat($entetem, $annoncemail);
    //$f_dest = fopen($annoncemail, "a+");
    fwrite($f_mail, "\nProgramme consultable sur le serveur\n");
    fwrite($f_mail, "  $actulimsi");
    fwrite($f_mail, "\nEt pour les séminaires passés");
    fwrite($f_mail, "\n  $urlout/$semdelannee");
}

/* Ajoute une ligne d'annonce dans la page LIMSI et dans LIMSI_INFO */

function ligne_limsi() {
    global $f_liste, $f_info, $urlout, $page, $quand, $quoi, $ou;
    global $auteurs, $affiliation, $semtitre;

    $lien = "\"$urlout/$page\"";
    //PAGE WEB
    $jour = datefr($quand);
    $heure = traduitime($quand);
    $span = "<span class=\"semdatelieu\">";
    fwrite($f_liste, "<br>&nbsp;<br><li>$span Le $jour &agrave; $heure - ");
    fwrite($f_liste, "$ou </span><br/>\n");
    fwrite($f_liste, "<strong>$auteurs</strong>\n");
    if ($affiliation) {
        fwrite($f_liste, " ($affiliation)\n ");
    }
    $ligne = "<br>&laquo; <a href=$lien>$semtitre</a> &raquo;\n";
    fwrite($f_liste, $ligne);

    //LIMSI INFO
    //$jour=datefr($quand);
    //$heure=traduitime($quand);
    fwrite($f_info, "<p>$jour, à $heure, $ou<br>\n");
    fwrite($f_info, "<b>$auteurs</b>\n");
    if ($affiliation) {
        fwrite($f_info, " ($affiliation) ");
    }
    fwrite($f_info, "<br><a href=$lien>$semtitre</a></p>\n\n");
}

function maj_statut($f_statut, $quand) {
    global $titre;

    $yenabiento = donnestatut($quand);
    fwrite($f_statut, "$yenabiento\n");

    if ($yenabiento) {
        $message = $titre[$yenabiento];
    } else {
        $message = "Prochains séminaires";
    }

    return $message;
}

/* Ajout dans la page envoyée par email */

function ligne_mail($file, $yenabiento) {
    global $quand, $quoi, $page, $urlout, $ou, $titre, $special;
    global $auteurs, $affiliation, $semtitre, $resume, $plan;

    $jour = datefr($quand);
    $heure = traduitime($quand);

    $saut = "\n\n";
    if (!empty($special)) {
        fwrite($file, $saut);
        fwrite($file, $special);
        //fwrite($file, $saut);
        fwrite($file, $saut);
    }

    fwrite($file, "$jour à $heure, $ou");
    fwrite($file, "\nAccès : $plan \n");
    fwrite($file, $saut);
    fwrite($file, $semtitre);
    fwrite($file, $saut);
    fwrite($file, "$auteurs\n$affiliation");


    // On ne met le resumé que pour les séminaires avec 1 2 3
    if ($yenabiento > 0 && $yenabiento <= 4) {
        fwrite($file, $saut);

        $resumef = textemail($resume, 80);
        fwrite($file, $resumef);
        fwrite($file, $saut);
    }

    
    
    fwrite($file, "\nAnnonce de la présentation à l'adresse \n");
    fwrite($file, "$urlout/$page\n");
    
    
}

function ligne_fast() {
    global $f_fast, $quand, $urlout, $page, $quoi, $ou, $where;
    global $auteurs, $affiliation, $semtitre;

    $jour = datefr($quand);
    $heure = traduitime($quand);

    $version_en =  dateen($quand) . ", " . $where;

    $fr_html = htmlentities("$jour à $heure, $ou");
    fwrite($f_fast, "<?translate(\"$fr_html\" , ");
    $en_html = htmlentities("$version_en");
    fwrite($f_fast, "\"$en_html\")?>:<br>");
    
    $fr_html = "\n<b>" . htmlentities($auteurs) ."</b>";
     if ($affiliation) {
         $fr_html .= "("  . htmlentities($affiliation) . "):<br>";
     };
    
     fwrite($f_fast, $fr_html);
     
     $fr_html = htmlentities($semtitre);
     /*
    fwrite($f_fast, "\n<b>$auteurs</b>");
    if ($affiliation) {
        fwrite($f_fast, " ($affiliation):<br>");
    }*/
     
    fwrite($f_fast, "\n<a href=\"$urlout/$page\">$fr_html");
    fwrite($f_fast, "</a><br>\n\n");
}

function ajouteimage($url) {
    $lien = "";
    if ($url != "") {
        $lien = "<img src=\"$url\" width=\"800\">\n";
    }
    return $lien;
}

function construitpage($f_page) {
    global $quand, $ou, $auteurs, $affiliation, $semtitre, $resume, $imgorg, $legende;
    global $plan;
    /*    global $entete,$bas,$dossier,$quand,$ou,$page,$num;
      global $auteurs,$affiliation,$semtitre,$resume;

      $affiche="$dossier/$page";
      $f_page=fopen($affiche,"a+");
      copy($entete,$affiche);
     */
    $jour = datefr($quand);
    $heure = traduitime($quand);
    fwrite($f_page, "<h2>Le $jour à $heure - $ou</h2>\n");
    fwrite($f_page, "<h3>$semtitre</h3>\n");
    fwrite($f_page, "<h2>$auteurs<br>\n");
    fwrite($f_page, "$affiliation</h2>\n");
    fwrite($f_page, "<p>$resume</p>\n");
    $lienimg = ajouteimage($imgorg);
    fwrite($f_page, $lienimg);
    fwrite($f_page, "<p>$legende</p>");
    fwrite($f_page, "<p><a href=\"$plan\">Accès $ou</a></p>");
}

/*
  ======================================================================
  Programme principal
  =========================================================================
 */


// Initialisation des fichiers
//liste.txt
$out = "$dossier/$sortie";


$f_liste = debut_limsi();

// Pour les envois individuels
$boitemail = "$essai/$mbox";

$f_box = fopen($boitemail, "w");
$f_ml = fopen("$essai/$mboxliste", "w");

//annonce par mail
$f_mail = debut_mail();


$f_statut = fopen($statut, "w");

//limsi info
$pageinfo = "$dossier/$limsinfo";
//copy($entetevide, $pageinfo);
$f_info = fopen($pageinfo, "w");



//html pour le fast
$pagefast = "$dossier/$fast";
$f_fast = fopen($pagefast, "w");



//on initialise
// Pour savoir à quel moment on met "Prochain Séminaires"
$balise = 0;
$sep = "\n----------------------------\n";

//On parcourt le tableau
//while ($seminaire = mysql_fetch_object($prochains)) {
while ($seminaire = $prochains->fetch_object()) {

    /* Données récupérées "brutes" */

    //numéro de la conf
    $num = $seminaire->id;

    // Titre général
    $quoi = $seminaire->name;
    // Début 
    $quand = $seminaire->start_time;
    // Identifiant de la salle
    $id_ou = $seminaire->room_id;

    // Champs additionnels
    $description = $seminaire->overload_desc;

    $organisateur = $seminaire->create_by;

    // Nom du fichier de l'affiche
    $page = nomfic($quand, $num);

    $plan = $lien_acces[$id_ou];

    //description de la salle
    $tab_tmp = recherchesalle($id_ou);
    $salle = $tab_tmp->fetch_array();
    $ou = $salle["description"];
    $where = $salle_en [$id_ou];

    //tableau où l'on découpe toutes les descriptions supplémentaires
    $tableau_champs = traduit_desc($description,"");

    $semtitre = $tableau_champs[$keskonveut['titre']];
    $auteurs = $tableau_champs[$keskonveut['auteur']];
    $affiliation = $tableau_champs[$keskonveut['affiliation']];
    $resume = $tableau_champs[$keskonveut['resume']];
    $imgorg = $tableau_champs[$keskonveut['image']];
     $legende = $tableau_champs[$keskonveut['legende']];
    $special = $tableau_champs[$keskonveut['special']];
    $mailinvitant = $tableau_champs[$keskonveut['invitant']];
    $mailverif = mail_orga($organisateur);


    // on teste pour savoir si c'est dans le mois
    $test = donnestatut($quand);
    // on ajoute le numéro dans le fichier de statut et on récupère le titre
    $interligne = maj_statut($f_statut, $quand);

    if ($test) {
        ligne_limsi();
        ligne_fast();

        if ($interligne) {
            fwrite($f_mail, $interligne);
            fwrite($f_mail, $sep);
        }

        ligne_mail($f_mail, $test);
        fwrite($f_mail, $sep);
    }

    $balise = $test;

    // Construction de la page .html
    $affiche = "$dossier/$page";
    copy($entete, $affiche);
    $f_page = fopen($affiche, "a+");

    construitpage($f_page);

    fclose($f_page);
    cat($bas, $affiche);


    // Construction du fichier mail
    $message_tmp = mailtmp($num);

    fwrite($f_box, "<a href=\"$urltest/index.php?detail=$num\">$num</a>\n");
    fwrite($f_ml, "$num,$mailverif,$essai/$message_tmp\n");
    $f_mailtmp = fopen("$essai/$message_tmp", "w");
    fwrite($f_mailtmp, "From: =?UTF-8?Q?S=c3=a9minaires_m=c3=a9canique_d=27Orsay?= <semmeca.info@limsi.fr>\n");
    fwrite($f_mailtmp, "To: <$mailverif>\n");
    fwrite($f_mailtmp, "Subject: Vérification annonce $num\n");
    fwrite($f_mailtmp, "Content-Type: text/plain; charset=utf-8; format=flowed\n");
    fwrite($f_mailtmp, "Content-Transfer-Encoding: 8bit\nContent-Language: fr\n\n");
    ligne_mail($f_mailtmp, 1);
    fclose($f_mailtmp);
}


fwrite($f_liste, "\n</ul>");
fclose($f_liste);
signature_mail();
fclose($f_mail);
fclose($f_statut);
fclose($f_info);
fclose($f_box);
fclose($f_ml);

// On copie liste.txt dans la page index.html
$tmp = "$dossier/$index";
copy($entete, $tmp);
cat($out, $tmp);
cat($bas, $tmp);

// On termine le code html de LIMSI-info
cat($bas, $pageinfo);
?>
