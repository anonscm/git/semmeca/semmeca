
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8">
  <title>Séminaires de mécanique d'Orsay</title>
  <meta name="viewport" content="width=device-width,initial-scale=1"/>
  <link rel="stylesheet" href="/modeles/style3.css">
  <link rel="stylesheet" href="/modeles/impression.css" media="print">
</head>
<body>
<IMG ALT="Logos LIMSI & FAST" src="/modeles/SemMeca-s.png"><P>
<div id="contenu">

<?php
# Configuratation générale (urls etc...)
require_once('config/general.inc.php');

# Paramètre des envois de mail (est-ce encore urile ?)
require_once('config/mail.inc.php');

# Fonctions diverses
require_once('utilitaires.php');

require_once('recupdonnees.php');
require_once("listeannonce.php");
require_once("historique.php");
?>

<h2>1 - Mise à jour</h2>
<ul>
    <li>Mettre comme type "Annonce" pour qu'un séminaire apparaisse dans la liste des prochains séminaires.
    </li>
</ul>
<p> Lien GRR : 
<?php echo "URL : $grrurl" ?>
</p>
<!--
<iframe src="<?php echo $grrurl ?>" width="900" height="800" name="grr"> </iframe>
-->
<h3>Historique : 
<?php
echo "<a href=\"$urlout/$semdelannee\" target=\"ailleurs\">";
echo date("Y");
echo "</a>";
?>
    </h3>
<h2>2 - Vérification des annonces</h2>
<ul>
    <li>Rechargez cette page pour mettre à jour les différentes annonces des prochains séminaires.
</ul>

<h3>LISN :</h3>
<p> Lettre :</p>
<div id="limsiinfo">
    <?php include "$dossier/$limsinfo" ?>
</div>
<p> site web : </p>
<div id="limsiinfo">
    <?php include "$dossier/$sortie" ?>
</div>

<h3>FAST : </h3>
<div id="limsiinfo">
    <?php include "$dossier/$fast" ?>
</div>

<h2>3 - Gestion des envois de mail</h2>

<p>
Un email annonçant les prochains séminaires est envoyé tous les vendredi à 11h. 
</p>
<p>
    En cas de changement, il est possible de copie-coller le contenu ci-dessous : 
<pre id="extraitmail">
<?php include "$dossier/$message"?> 
</pre>
<h3>Annonces par séminaire</h3>
    <?php
    include $boitemail;
    ?>
</p>






<p id="inactif">Version 3 - 28/12/2023</p>

</body>
</html>
