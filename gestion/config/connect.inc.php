<?php

#L' utilisateur grrspy ne peut pas modifier les tables grr.
#Il ne fait que lire.
#Les variables ont le même nom que dans connect.inc.php
$dbUser = "";
$dbPass = ".";

# Serveur de grr
$dbHost = "";

#base de donnees de grr
$dbDb = "";

/* Table où sont stockées les conférences */
$table = "pl1_entry";
$listesalle = "pl1_room";

/* correspondance id (pl1-overload) et champs recherché */

$keskonveut = array('auteur' => 2,
    'affiliation' => 4,
    'titre' => 1,
    'invitant' => 6,
    'resume' => 8,
    'special' => 7,
    'image' => 9,
    'legende'=>10);


# Pour chaque salle, le lien vers la page web expliquant l'accès
$lien_acces = array(
    '1' => "https://www.limsi.fr/fr/acces",
    '2' => "http://www.fast.u-psud.fr/fr/plan",
    '6' => "https://www.limsi.fr/fr/acces",
    '5' => "http://lptms.u-psud.fr/locations/amphi-blandin-du-lps-de-la-faculte-des-sciences-dorsay-batiment-510/",
    '7' => "http://geops.geol.u-psud.fr/spip.php?rubrique37"
);

# Le nom en anglais pour affichage sur version EN du site
$salle_en = array(
    '1' => " LIMSI conference room",
    '2' => "FAST conference room",
    '6' => "LIMSI board room",
    '5' => "Blandin auditorium",
    '7' => "GEOPS"
        )
?>
