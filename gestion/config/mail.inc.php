<?php

/* configuration pour tout ce qui concerne le mail */

// Fichier indiquant le statut des messages (1, 2, 3, 4)
$statut = "$dossier/prochains.seminaires";

// Valeur maximale du statut
$maxstatut = 5;

/* 
On teste la différence entre la date actuelle et celle du séminaire

Si un séminaire a lieu entre mintemps et maxtemps (en heures), on indique la variable correspondante dans le fichier de statut. 
Par exemple : si un séminaire a lieu entre 2 et 12h, on écrit "1" dans le fichier de statut et on indique "Cet après-midi"
*/

//A 11h, tous les jours, on verifie s'il y a une conf avec le statut "1" (cette après-midi) 
$mintemps[1]= 1;
$maxtemps[1]= 6;
// Le titre qui apparait en interligne dans le mail
$titre[1] = "";
// Les destinataires du message
$destinataires[1]="semmeca.campus@mondomaine.fr";
// Le sujet du message
$sujetmail[1]="Cet après-midi";
// L'adresse de la liste
$sympa[1]="https://sympa.mondomaine.fr/sympa/info/semmeca.campus";
// Une information sur la crontab (à installer sur rubens)
$infocron[1]="11h tous les jours";

// Demain
$mintemps[2]= 6;
$maxtemps[2]= 24;
$titre[2] = "";
$destinataires[2]="";
$sujetmail[2]="";
$sympa[2]="";
$infocron[2]="";

/*A 15h, tous les jours, on vérifie s'il y a un séminaire dans les 2 jours (statut "3") */
$mintemps[3]= 24;
$maxtemps[3]= 48;
$titre[3] = "";
$destinataires[3]="semmeca.plateau@mondmaine.fr";
$sujetmail[3]="Après-demain";
$sympa[3]="https://sympa.mondmaine.fr/sympa/info/semmeca.plateau";
$infocron[3]="15h tous les jours";


/* Le jeudi après le séminaire, on envoie un message indiquant le séminaire de la semaine prochaine (7*24 = 168) */
$mintemps[4]= 48;
$maxtemps[4]= 168;
$titre[4] = "";
$destinataires[4]="semmeca.large@mondmaine.fr";
$sujetmail[4]="Séminaire Mécanique d'Orsay";
$sympa[4]="https://sympa.mondmaine.fr/sympa/info/semmeca.large";
$infocron[4]="15h le jeudi";

/* On teste les annonces prochaines : envoi aux organisateurs (35*24=744) */
$mintemps[5]= 168;
$maxtemps[5]= 850;
$titre[5] = "";
$destinataires[5]="";
$sujetmail[5]="";
$sympa[5]="";
$infocron[5]="";
