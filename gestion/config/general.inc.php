<?php

// url des dossiers temporaires
$urlout="https://www.limsi.fr/affiche";

// Répertoire où l'on créé les pages
$dossier="/web/upload/semmeca/affiche";
// Répertoire où sont stockés les éléments graphiques
// Ne se trouve pas sous Git. Peut être édité
$modeles="/var/www/semmeca/modelesUTF8";

// Entete et pied des fichiers
// A créer / Personnaliser
$entete="$modeles/entete.html";
$enteteg="$modeles/enteteg.html";
$entetem="$modeles/entetem.txt"; 
$entetevide="$modeles/vide.html";
$bas="$modeles/bas.html";

// Comment atteindre GRR
$grrurl="https://www.monsite.fr/planning/month_all.php?area=2";
$grrview="$grrurl/view_entry.php";
$grrlien="<a href=\"$grrurl\">GRR</a>";

// POur avoir la date en Français
setlocale(LC_TIME, "fr_FR.utf8");

// gestion des annonces
// Page d'accueil
$index="index.html";


// Code LIMSI
$sortie="liste.txt";
$actulimsi="https://www.limsi.fr/fr/actualites/447-seminaire-de-mecanique-d-orsay";

// Code FAST
$fast="codefast.txt";


// Annonce par email
$message="annoncemail.txt";

// Test d'envoi des prochains messages
$mbox = "mbox.html";
$mboxliste = "mbox.liste";

// url des dossiers temporaires
$urltest="https://www.monsite.fr/essai";

// Répertoire où l'on créé les pages
$essai="/web/upload/semmeca/essai";

// Agenda de la page d'accueil
$limsinfo="agenda.txt";

?>
