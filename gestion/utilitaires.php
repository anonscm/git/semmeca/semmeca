<?php
/* Fonctions générales appelées dans plusieurs scripts */

// Cette fonction ouvre en lecture $source
// recopie le contenu dans $dest.
function cat($source,$dest) {
  if (file_exists("$source")) {
    $data = file_get_contents($source);
    $f_dest=fopen($dest,"a+");
    fwrite($f_dest, $data);
    fclose($f_dest);
  } else {
    echo "Erreur lecture $source";
  };  
};


/* Gestion du temps */


/* Test si c'est du passe */
function testpasse($quand) {
$maintenant = time();
$diff=$quand-$maintenant;
 if ($diff < 0) {
  return 0;
 } else {
  return 1;
 }
}

function donnestatut($quand){
  global $mintemps, $maxtemps, $maxstatut;

  $maintenant = time();
  $diff = $quand - $maintenant;

  // retour du code
  $code = 0;

  for ($i=1; $i<=$maxstatut; $i++) {
   $min= ($mintemps[$i] * 60 * 60);
   $max= ($maxtemps[$i] * 60 * 60);
   if (($diff >= $min) && ($diff < $max)) {
    $code = $i;
   } 
  } 
  
  return $code;
}

/* Traduit $present en 13/01/2011 */
function traduitdate($present){
#    setlocale(LC_TIME, "fr_FR.utf8");
#  Deprecated   
    $jour   = strftime("%d", $present);
    $mois   = strftime("%m", $present);
    $annee  = strftime("%Y", $present);
    $jma  = "$jour/$mois/$annee";
    return $jma;
}

/* calcule le nom du fichier */
function nomfic($present,$id){
    $jour   = strftime("%d", $present);
    $mois   = strftime("%m", $present);
    $annee  = strftime("%Y", $present);
    $am   = strftime("%H", $present);
    $fic  = "affiche$id.$mois$annee.html";
    $fic = $annee.$mois.$jour."_".$am.".html";
    return $fic;
}

function mailtmp($id){
    $fic = "message_$id.txt";
    return $fic;
}

function nomhist($present,$id){
    setlocale(LC_TIME, "fr_FR.utf8");
    $annee  = strftime("%y", $present);
    $fic  = "res".$annee."_$id.html";
    return $fic;
}

/* calcule l'URL des séminaires passés */
function sempasse($present){
    setlocale(LC_TIME, "fr_FR.utf8");
    $annee  = strftime("%Y", $present);
    $fic  = "SemPla$annee.html";
    return $fic;
}

/* Affiche la date en français */
function datefr($present){
	$formatter = new IntlDateFormatter('fr_FR', IntlDateFormatter::LONG, IntlDateFormatter::NONE);
    $jm = $formatter->format($present);
    return $jm;
}

/* donne l'heure+minute */
function traduitime($present) {
    $heure = strftime("%H", $present);
    $min = strftime("%M", $present);
    $quand = $heure."h".$min;
    return $quand;
}

/* calcule le timestamp unix de l'ann�e */
function debutannee() {
    $annee=date("Y");
    $nombre = mktime(0,0,0,1,1,$annee);
    return $nombre;
}

function finannee() {
    /* Test pour que les séminaires 2013 s'affichent */
    $annee=date("Y")+1;
    $nombre = mktime(0,0,0,1,1,$annee);
    return $nombre;
}

/* Affiche la date pour l'historique */
function datehist($present){
	$formatter = new IntlDateFormatter('fr_FR', IntlDateFormatter::LONG, IntlDateFormatter::NONE);
    $jm = $formatter->format($present);
    return $jm;
}

function dateen($present) {
    setlocale(LC_TIME, "C");
    $joursem  = strftime("%A", $present);
    $jour  = strftime("%e", $present);
    $mois   = strftime("%B", $present);
    $heure = strftime("%H", $present);
    $jm  = "$joursem, $mois $jour at $heure";
    setlocale(LC_TIME, "fr_FR.utf8");
    return $jm;
}

function textemail($texte,$longueur) {
    // On remplace les <br> par des retours à la ligne
    $tmp1 = str_replace('<br>', "\n\n", $texte);
    
    // On enlève tous les caractères html
    $tmp2 = strip_tags($tmp1);
    
    // S'il y a une longeur donnée, on coupe à cette longeur
    if (isset($longueur)) $tmp1 = wordwrap($tmp2, $longueur, "\n");
            else $tmp1 = $tmp2;
    
    return $tmp1;
    
}
?>
