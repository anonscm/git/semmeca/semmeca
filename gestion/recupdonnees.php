<?php


/* Recherche de toutes les informations concernant un id */

function recherchepage($identifiant) {
  global $table;
  $question="select * from $table where `id`=$identifiant";
  echo "<!--$question-->";
  $reponse=mysql_query("$question");
  if (!$reponse) {
    die ("$req_listeid :".mysql_error());
  }
  return $reponse;
}

// Retourne la description d'une salle
function recherchesalle($idsalle) {
  global $listesalle,$milou;
  $question="SELECT `description` FROM $listesalle where `id`=$idsalle";
  //echo "<!--recupdonnees.php  : ligne 21 $question-->";
  $reponse=$milou->query("$question");
  if (!$reponse) {
    die ("ERREUR RD1 :".$milou->error);
  } else {
  //echo "<!--recupdonnees.php  : ligne 26 ";
  //print_r($reponse);
  //echo " -->";
  return $reponse;
  }
}


/* Pour rechercher la corrspondance, id -> titre */

// On créé le tableau comprenant tous les id
function id_filename() {
  global $milou;
  $question=" SELECT `id` , `fieldname` FROM `pl1_overload` order by id ASC";
  $reponse=$milou->query("$question");
  if (!$reponse) {
    die ("ERREUR RD2 :".$milou->error);
  }
  $i=0;
  while ($num=$reponse->fetch_object()) {
   $i++;
   $id_desc[$i]=$num->id;
  }
//  echo "\n<!--recupdonnees.php  : ligne 49 reponse";
//  print_r($reponse);
//  echo " -->\n";
  return $id_desc;
}

function nettoie($texte) {
 $tmp1=stripcslashes($texte);
 $tmp2=stripcslashes($tmp1);
 while ($tmp1 != $tmp2) {
     // tant qu'il y a des slashes à enlever
     $tmp1 = $tmp2;
     $tmp2=stripcslashes($tmp1);
 }
 return $tmp2;
}


/* inspiré de mrbsEntryGetOverloadDesc() 
/var/www/semmeca/include/mrbs_sql.inc.php*/
function traduit_desc($megaligne,$decode) {
  $overload_array = array();
  global $keskonveux;
  $texte=nettoie(urldecode($megaligne));
  if ((isset($decode)) and ($decode == "ISO")) {
      $texte=  utf8_encode($texte);
  };
  $liste=id_filename();
  foreach ($liste  as $num) {
    $begin_string = "@".$liste[$num]."@";  // @1@
    $end_string = "@/".$liste[$num]."@"; // @/1@
    //print "<!-- $num  $begin_string -->\n";

    $chaine = $texte;
    $l1 = strlen($begin_string); // 3 ou 4 (pour 10)
    $l2 = strlen($end_string); // 4 ou 5 (pour 10)

    $balise_fermante='n'; // initialisation
    $balise_ouvrante='n'; // initialisation
    $traitement1=true; // initialisation
    $traitement2=true; // initialisation

    while(($traitement1!==false) or ($traitement2!==false)) {

      // cherche @1@ et retourne ce qui suit
      if ($traitement1!=false) {
       $chaine1 = strstr ( $chaine  , $begin_string  ); // /@1@truc@/1@@2@bidule
       if ($chaine1!==false) {  // on a trouve @1@
         $balise_ouvrante='y';
         $chaine = substr($chaine1,$l1,strlen($chaine1)-$l1); //truc@/1@@2@bidule
	 $result=$chaine;
       } else {
	 $traitement1=false; // c'est fait, on ne recommence plus
       }
      }
      
      if ($traitement2!=false) { // on n'a pas trouvé @/1@
	$ind=0;
        $end_pos=true;
	while ($end_pos!==false) {
                $end_pos = strpos($chaine,$end_string,$ind); // On repère
                if ($end_pos !==false) { // on a trouvé @/1@
                   $balise_fermante='y';
                   $ind_old = $end_pos;
                   $ind = $end_pos+$l2;
                } else
                   break; // on sort de la boucle
         }
        if ($ind != 0 ) {
                 $chaine = substr($chaine,0,$ind_old);
                 $result = $chaine; // On mémorise la valeur précédente
        } else 
                 $traitement2=false;
       } // fin if ($traitement2!=false)

      } // fin du while, $traitement2 et $traitement1 sont faux

    
   if (($balise_fermante=='n' ) or ($balise_ouvrante=='n'))
    $overload_array[$num]='';
   else
    $overload_array[$num]=nettoie($result);
  } // fin du foreach

  return $overload_array;
}


/* Mail de l'organisateur */
function mail_orga($login) {
  global $milou;
    $adresse = "$login@limsi.fr";
    
    $question=" SELECT `email` FROM `pl1_utilisateurs` WHERE `login` LIKE '$login'";
 
    $reponse=$milou->query("$question");
    
    if (!$reponse) {
        die ("ERREUR RD3 :".$milou->error());
    } 
    
    while ($result= $reponse->fetch_object())
        $adresse = $result->email;
    
    return $adresse;
}

/* Lecture du fichier de configuration : */

if (file_exists("config/connect.inc.php"))
  {
   require_once("config/connect.inc.php");
 } 
else
 {
   echo "Il manque le fichier config/connect.inc.php";
   exit;
 }

/* test de la connexion */
#$milou = mysql_connect("$dbHost", "$dbUser", "$dbPass")
$milou = new mysqli("$dbHost", "$dbUser", "$dbPass","$dbDb");

if ($milou->connect_error)
 die("Problème de connexion à $dbHost ($dbUser)");


/* Pour lister les identifiants des conférences avec le statut Annonce */
$champs = " `id` , `start_time` , `room_id` , `name` , `type` , `description` , `overload_desc`, `create_by`";


/* 
On ne sélectionne que les séminaires prochains
Si on veut que ceux de l'année en cours (history), on utilise debutannee()
Remplacer E par une autre lettre si l'on veut afficher autre chose */

$limite=time();
$condition_temps = "`start_time` > $limite";
// On ne veut pas voir les séminaires passés
//$condition_type = "`type` LIKE CONVERT( _utf8 'E' USING latin1)";
$condition_type = "`type` LIKE 'E'";
$condition="$condition_temps and $condition_type";
$ordre="order by `start_time` ASC";
$req_listeid = "select $champs from $table where $condition";

echo "<!-- $req_listeid -->";
//prochains séminaires (sans condition d'année)

/* A Mettre à jour 
$prochains = mysql_query("$req_listeid");
*/
$prochains = $milou->query("$req_listeid");

if (!$prochains) {
  die ("$req_listeid :".$milou->error);
}

// Pour construire la page "séminaires de l'année..."
$limite=debutannee();
$condition_temps = "`start_time` > $limite";
$limite=finannee();
$condition_temps .= " AND `start_time` < $limite";
$condition="$condition_temps and $condition_type";
$ordre="ORDER BY `start_time` DESC";
$req_historique = "select $champs from $table where $condition $ordre";

//séminaires de l'année

$touteannee = $milou->query("$req_historique");
if (!$touteannee) {
  die ("$req_historique :".$milou->error);
}

?>
