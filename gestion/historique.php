<?php

/*
  ======================================================================
  Programme principal
  =========================================================================
 */

// Calcul du nom du fichier
$semdelannee = sempasse(time());
$historique = "$dossier/$semdelannee";
copy($entete, $historique);
$f_dest = fopen($historique, "a+");

$nbsem = $touteannee->num_rows;
fwrite($f_dest, "<h2> Ann&eacute;e ");
fwrite($f_dest, date("Y"));
fwrite($f_dest, "</h2>\n <table>");

/*
 * //On parcourt le tableau
 */
$numhist = $nbsem + 1;

while ($seminaire = $touteannee->fetch_object()) {

    $numhist = $numhist - 1;
    //Date
    $quand = $seminaire->start_time;

    $jour = datehist($quand);



    //Titre avec lien
    $num = $seminaire->id;


    $description = $seminaire->overload_desc;



    $pagehist = nomhist($quand, $numhist);
    $lienhist = "\"$urlout/$pagehist\"";
    if ($quand < 1504785600) {
        $tableau_champs = traduit_desc($description, "ISO");
    } else {
        $tableau_champs = traduit_desc($description);
    }


    $semtitre = $tableau_champs[$keskonveut['titre']];


    // Identifiant de la salle
    $id_ou = $seminaire->room_id;
  $plan = $lien_acces[$id_ou];
    //description de la salle
    $tab_tmp = recherchesalle($id_ou);
    $salle = $tab_tmp->fetch_array();
    $ou = $salle["description"];

    //Orateur
    $auteurs = $tableau_champs[$keskonveut['auteur']];

    // Labo
    $affiliation = $tableau_champs[$keskonveut['affiliation']];

    fwrite($f_dest, "\n\n<p>$jour : \n");
        fwrite($f_dest, "<b>$auteurs</b>\n");
    fwrite($f_dest, "($affiliation)<br/>\n");
    fwrite($f_dest, "<a href=$lienhist>$semtitre</a></p>\n");

    $resume = $tableau_champs[$keskonveut['resume']];
    $imgorg = $tableau_champs[$keskonveut['image']];
    $legende = $tableau_champs[$keskonveut['legende']];

    $affichehist = "$dossier/$pagehist";
    copy($entete, $affichehist);
    $f_hist = fopen($affichehist, "a+");


    /* correctif -> UTF8 */
    $semtitre = htmlentities($semtitre);

    construitpage($f_hist);
    fclose($f_hist);
    cat($bas, $affichehist);
}

fwrite($f_dest, "\n</table>");

$avant = date("Y") - 1;

$titrelien = "<a href=\"SemPla$avant.html\">";
$titrelien .= "R&eacute;capitulatif des s&eacute;minaires $avant</a>";

fwrite($f_dest, "<h3> $titrelien</h3>");

fclose($f_dest);
cat($bas, $historique);


?>
