--------------------
Dossier gestion
-------------------

Les fichiers du répertoire config sont à modifier

index.php affiche juste les liens vers : 
- GRR (planning)
- Les scripts => main.php
- Les listes Sympa

main.php est le contrôleur. 

Il appelle les fichiers de config (à mettre à jour) : 
- config/general.inc.php
- config/mail.inc.php

Un fichier contenant des fonctions basiques
- utilitaires.php

Le modèle pour récupérer les données de GRR
- recupdonnees.php

Vue : affichage de la page d'annonce et des textes des mails
- listeannonce.php

Affiche la liste des séminaires passés
- historique.php

Des exemples de modèle pour le haut et le bas des pages est donné dans le dossier modeles. Ils sont à adapter. 
