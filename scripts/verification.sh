#!/bin/bash
# A installer dans une crontab (pas forc�ment sur le serveur web).
# Message de v�rification envoy� � la personne qui invite. 
#00 8 * * * toto /usr/local/bin/semmeca/verification.sh


# O� on logue les envois, messages en attente, etc
log="/var/log/semmeca/verification.log"

# Liste des messages a envoyer
listedist="https://www.monsite.fr/essai/mbox.liste"

# Ou se trouve la liste
listeloc="/var/log/semmeca/mbox.liste"

# L'URL a mettre � jour
url="https://www.monsite.fr/gestion/"

# Fichiers temporaires
recup="/tmp/recup.liste"
deja="/tmp/deja.tmp"
message="/tmp/message.txt"

quand="$(date +%c)"

litliste() {
 while read ligne $fichier; do
 grep $ligne $log > $deja
 if [ -s $deja ]; then
 	echo -n "$quand : message deja envoy� : " >> $log
	echo $ligne | cut -d',' -f1 >> $log
	rm $deja
 else 
	echo "$quand : $ligne" >> $log
	echo $ligne | cut -d',' -f3 >> $recup
 fi
 done
}

envoi() {
 while read adresse $fichier; do
 wget -O  $message -q $adresse
 /usr/lib/sendmail -t < $message
 done
}

#On met � jour l'URL 
wget -O /dev/null --no-check-certificate -q $url

#On r�cup�re la liste des messages "en cours"
wget -O $listeloc --no-check-certificate -q $listedist

litliste < $listeloc

if [ -s $recup ]; then
	envoi < $recup
	rm $recup
fi
