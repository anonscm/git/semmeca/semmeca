--------------------
Dossier gestion
-------------------

Ces scripts envoient des mails et mettent à jour les pages web. 
Ils peuvent être placés sur n'importe quel serveur (pas forcément celui qui affiche les pages web).

Le dossier mailing.sh est appelé avec un paramètre défini dans /gestion/confif/mail.inc.php

#Exemple de crontab (remplacer toto par n'importe quel utilisateur, pas besoin de droits root)

# Message de vérification envoyé à la personne qui invite. 
00 8 * * * toto /usr/local/bin/semmeca/verification.sh

# Semmeca.campus si seminaire cette après-midi
00 11 * * * toto /usr/local/bin/semmeca/mailing.sh 1

# Semmeca.plateau si seminaire après-demain
00 15 * * 0,1,2,3,6  toto /usr/local/bin/semmeca/mailing.sh 3

# Semmeca.large tous les jeudi
00 15 * * 4 toto /usr/local/bin/semmeca/mailing.sh 4

