#!/bin/bash

if [ $1 ]; then 
  code=$1
else
  echo "Il manque le code"
  exit
fi

message="/tmp/mail.txt"
tmp="/tmp/semmeca.txt"

texte="https://www.monsite.fr/affiche/annoncemail.txt"
statut="https://www.monsite.fr/affiche/prochains.seminaires"
log="/var/log/semmeca/mailing.log"
url="https://www.monsite.fr/gestion/"
confmail="/usr/local/bin/semmeca/mail.inc.php"

#On met � jour l'URL 
wget -O /dev/null  --no-check-certificate  -q $url

wget -O $tmp  --no-check-certificate  -q $statut
#grep "^$code$" $statut > $message
grep "^$code$" $tmp > $message

quand="$(date +%F) : $(date +%H)h";

if [ -s $message ]; then 
  echo -n "$quand : $code : " >> $log
else
  echo "$quand : $code : RAS" >> $log
  exit 
fi 

echo "From: S�minaire M�canique d'Orsay <semmeca_info@mondomaine.fr>" > $message
echo -n "To: ">> $message
quoi="destinataires"
grep $quoi $confmail | grep $code | cut -d'"' -f2 >> $message
grep $quoi $confmail | grep $code | cut -d'"' -f2 >> $log

echo -n "Subject: ">> $message
quoi="sujetmail\[$code"
#grep $quoi $confmail | grep $code | cut -d'"' -f2 >> $message
grep $quoi $confmail  | cut -d'"' -f2 >> $message


echo "" >> $message
wget -O $tmp  --no-check-certificate -q $texte
cat $tmp >> $message

echo "" >> $message

/usr/lib/sendmail -t < $message

